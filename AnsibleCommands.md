generate ssh-key 

1. ssh-keygen -t rsa (client, server)
2. ssh-copy-id -i ansible.pub marc@remoteserver-ip (server)
3. ssh-add ~/.ssh/ansible

Host key checking
#this will scan the public key of the remote server and add to known hosts of the server
ssh-keyscan -H client-ip >> ~/.ssh/known_hosts

Disable key check - convinient for dynamically created and destroyed servers, set the below in config file

host_key_checking = False

Default config file path
/etc/ansible/ansible.cfg or ~/.ansible.cfg
You can also create the .cfg in project dir.


#test all host connection

-i = inventor , -m module

ansible all -i hosts -m ping

#target grouplet

ansible docker -i hosts -m ping

target only specific host

ansible 192.168.70.61 -i hosts -m ping

execute playbook
ansible-playbook -i hosts my-playbook.yaml

install the specific version
  tasks:
  - name: install nginx server
    apt:
      name: nginx=1.18-ubuntu
      state: present
for the state
  - present is for specific version
  - latest is for latest version
  - absent is for uninstalling the package

**Install collection**

ansible-galaxy collection list

ansible-galaxy collection install amazon.aws